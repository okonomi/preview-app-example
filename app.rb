require "sinatra"
require "sinatra/activerecord"

set :bind, "0.0.0.0"
port = ENV["PORT"] || "8080"
set :port, port

db_host = ENV["DB_HOST"] || "localhost"
db_port = ENV["DB_PORT"] || "3306"
db_user = ENV["DB_USER"] || "root"
db_pass = ENV["DB_PASSWORD"] || "my-secret-pw"
db_name = ENV["DB_NAME"] || "my_database"
db_socket = ENV["DB_SOCKET"]

set :database, {
  adapter: "trilogy",
  encoding: "utf8mb4",
  username: db_user,
  password: db_pass,
  # host: db_host,
  socket: db_socket,
  database: db_name
}

class Post < ActiveRecord::Base
end

asset_host_url = ENV["ASSET_HOST_URL"] || "http://localhost:8080"

get "/" do
  post = Post.first
  erb :index, locals: { asset_host_url:, post: }
end
