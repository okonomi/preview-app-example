#!/bin/bash
set -euo pipefail

bundle exec rake db:migrate
bundle exec rake db:seed:replant

bundle exec ruby app.rb
