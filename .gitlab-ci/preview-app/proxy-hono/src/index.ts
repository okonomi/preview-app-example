import { Hono } from 'hono'
import { getAuth, oidcAuthMiddleware, processOAuthCallback, revokeSession } from '@hono/oidc-auth'

type Bindings = {
  PREVIEW_APP_ORIGIN_DOMAIN: string
}

const app = new Hono<{ Bindings: Bindings }>()

app.get('/logout', async (c) => {
  await revokeSession(c)
  return c.text('You have been successfully logged out!')
})
app.get('/callback', async (c) => {
  return processOAuthCallback(c)
})
app.use('*', oidcAuthMiddleware())
app.use('*', async (c, next) => {
  const auth = await getAuth(c)
  if (!auth?.email.endsWith('@gmail.com')) {
    return c.text('Unauthorized', 401)
  }
  await next()
})
app.get('/', async (c) => {
  const url = new URL(c.req.url)
  url.hostname = c.env.PREVIEW_APP_ORIGIN_DOMAIN
  url.port = "443"

  const newRequest = new Request(url, c.req.raw)
  const response = await fetch(newRequest)
  return new Response(response.body, response)
})

export default app
