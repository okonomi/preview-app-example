variable "google_cloud_project" {
  description = "The id of google_cloud_project"

  type = string
}

variable "google_cloud_region" {
  type = string
}

variable "preview_app_name" {
  description = "The name of the preview app"
  
  type = string
}

variable "cloudflare_api_token" {
  description = "The API token for Cloudflare"

  type = string
}

variable "cloudflare_zone_id" {
  description = "The id of Cloudflare zone"

  type = string
}
