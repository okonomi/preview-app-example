output "repository_url" {
  value = "${google_artifact_registry_repository.default.location}-docker.pkg.dev/${google_artifact_registry_repository.default.project}/${google_artifact_registry_repository.default.repository_id}"
}

output "cloudbuild_bucket_url" {
  value = google_storage_bucket.cloudbuild_source.url
}

output "static_resource_bucket_url" {
  value = google_storage_bucket.static_resource.url
}

output "database_connection_name" {
  value = google_sql_database_instance.preview_app.connection_name
}

output "database_user_name" {
  sensitive = true
  value = google_sql_user.preview_app.name
}

output "database_user_password" {
  sensitive = true
  value = google_sql_user.preview_app.password
}
