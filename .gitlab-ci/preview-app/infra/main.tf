terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "5.25.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.30.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.6.1"
    }
  }

  backend "http" {
  }
}

provider "google" {
  project = var.google_cloud_project
  region  = var.google_cloud_region
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

provider "random" {
}

locals {
  # 最初の15文字を取得
  first_15 = substr(var.preview_app_name, 0, 15)

  # SHA-256ハッシュを計算し、最初の8文字を取得
  sha256_first_8 = substr(sha256(var.preview_app_name), 0, 8)

  # 最終的な文字列を結合
  preview_app_short_name = "${local.first_15}-${local.sha256_first_8}"
}

# Artifact Registryのリポジトリを作成
resource "google_artifact_registry_repository" "default" {
  repository_id = "${var.preview_app_name}-repository"
  location      = var.google_cloud_region
  format        = "DOCKER"
}

# Cloud Buildsのソースアップロード用GCSバケット
resource "google_storage_bucket" "cloudbuild_source" {
  name          = "${var.google_cloud_project}_${local.preview_app_short_name}_cloudbuild"
  location      = var.google_cloud_region
  force_destroy = true
}

# 静的リソース用のGCSバケット
resource "google_storage_bucket" "static_resource" {
  name          = "preview-${var.preview_app_name}-static.oknm.jp"
  location      = var.google_cloud_region
  force_destroy = true
}

resource "google_storage_bucket_iam_member" "static_resource" {
  bucket = google_storage_bucket.static_resource.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}

# 静的リソースのGCSバケットのDNSレコード
resource "cloudflare_record" "static_resource" {
  zone_id = var.cloudflare_zone_id
  name    = "preview-${var.preview_app_name}-static"
  type    = "CNAME"
  value   = "c.storage.googleapis.com"
  proxied = true
}

# WorkersのDNSレコード
resource "cloudflare_record" "workers" {
  zone_id = var.cloudflare_zone_id
  name    = "preview-${var.preview_app_name}-www"
  type    = "CNAME"
  value   = "preview-custom-domain-app.okonomi.workers.dev"
  proxied = true
}

# Cloudflare Accessをセットアップ
resource "cloudflare_access_application" "preview_app" {
  zone_id = var.cloudflare_zone_id
  name    = "preview-${var.preview_app_name}"
  type    = "self_hosted"

  self_hosted_domains = [
    "preview-${var.preview_app_name}-www.oknm.jp",
    "preview-${var.preview_app_name}-static.oknm.jp"
  ]

  http_only_cookie_attribute = true
}

resource "cloudflare_access_policy" "mailaddress" {
  application_id = cloudflare_access_application.preview_app.id
  zone_id        = var.cloudflare_zone_id
  name           = "mail address"
  decision       = "allow"
  precedence     = 1

  include {
    email = ["okonomi@oknm.jp"]
  }
}

# Cloud SQL
resource "google_sql_database_instance" "preview_app" {
  name             = "${var.preview_app_name}-db"
  database_version = "MYSQL_5_7"
  settings {
    tier                        = "db-f1-micro"
    edition                     = "ENTERPRISE"
    availability_type           = "ZONAL"
    deletion_protection_enabled = false
    disk_autoresize             = false
    disk_size                   = 10
    disk_type                   = "PD_HDD"
    backup_configuration {
      enabled = false
    }
    ip_configuration {
      ipv4_enabled    = true
      private_network = null
    }
  }

  deletion_protection = "false"
}

resource "google_sql_database" "preview_app" {
  name      = "my_database"
  instance  = google_sql_database_instance.preview_app.name
  charset   = "utf8mb4"
  collation = "utf8mb4_bin"
}

resource "random_password" "db_password" {
  length  = 16
  special = true
}

resource "google_sql_user" "preview_app" {
  name     = var.preview_app_name
  instance = google_sql_database_instance.preview_app.name
  host     = "%"
  password = random_password.db_password.result
}
